//
//  ViewController.swift
//  google
//
//  Created by korvincorey on 6/9/18.
//  Copyright © 2018 korvincorey. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleAPIClientForREST

class ViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
   
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheetsReadonly]
    private let service = GTLRSheetsService()
    let todayDate = Date(timeIntervalSinceNow: 36000)
    var usersName = String()
    var userIndex = Int()
    var arrayCounter = 0
    let getUrlString = "https://sheets.googleapis.com/v4/spreadsheets/1NrPDjp80_7venKB0OsIqZLrq47jbx9c-lrWILYJPS88"
    @IBOutlet weak var listView: UITextView!
    @IBOutlet weak var GoogleSI: GIDSignInButton!
    @IBOutlet weak var resultLabel: UILabel!
    @IBAction func signOutButton(_ sender: Any) {
        GIDSignIn.sharedInstance().signOut()
    }
    
    @IBAction func menuButton(_ sender: Any) {
        usersName = "[Оскар Робертсон]"
        let dateForLunch = dateForSheet()
        if dateForLunch == "вторник" {                      //add special check beacuse all week days have space after it's name, except Tuesday
            listOfDishes(range: "\(dateForLunch)!A3:A34")   // list of users
            listOfDishes(range: "\(dateForLunch)!\(userIndex):\(userIndex)")    // list of dishes
            //listOfDishes(range: "\(dateForLunch)!A:A")    // order row
        } else {
        listOfDishes(range: "\(dateForLunch) !A3:A34")
//        listOfDishes(range: "\(dateForLunch) !A:A")
//        listOfDishes(range: "\(dateForLunch) !A:A")
        }
    }
    
    func dateForSheet() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let strForDate = dateFormatter.string(from: todayDate as Date)
        return strForDate
    }
    
    func listOfDishes(range: String) {
        listView.text = "Getting sheet data..."
        let spreadsheetId = "1NrPDjp80_7venKB0OsIqZLrq47jbx9c-lrWILYJPS88"
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet
            .query(withSpreadsheetId: spreadsheetId, range:range)
        service.executeQuery(query,
                             delegate: self,
                             didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:))
        )
    }
    
    // Process the response and display output
    @objc func displayResultWithTicket(ticket: GTLRServiceTicket,
                                 finishedWithObject result : GTLRSheets_ValueRange,
                                 error : NSError?) {
        
        if let error = error {
            showAlert(title: "Error", message: error.localizedDescription)
            return
        }
        
        var dishesString = ""
        let cellsArray = result.values!
        print("gavno")
        
        if cellsArray.isEmpty {
            listView.text = "No data found."
            return
        }
        
        for cell in cellsArray {
            arrayCounter += 1
            var stringModifier = ""
            stringModifier += "\(cell)"
            if stringModifier == usersName {
                userIndex = arrayCounter + 2  //because of user list started from third row
                print("we got match! userIndex in Array is \(userIndex)")
            }
        }
        
        for cell in cellsArray {
            
        }
        listView.text = "we got match! userIndex in Array is \(userIndex)"
//        listView.text = dishesString
    }
    
    
    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            self.service.authorizer = nil
        } else {
            self.GoogleSI.isHidden = true
            self.listView.isHidden = false
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            //listMajors()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

